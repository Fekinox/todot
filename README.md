# todot

A shell-based todo list.

## About

`todot` is an extremely minimalistic todo list manager, taking up no more than 200 lines of shell script. It's simple enough to get the job done.

`todot` will keep track of its list in `$HOME/.local/share/todot`.

### Usage

```
# Supply all as a task name to specify all tasks.
# Supply dmenu as a task name to open a menu to select a task.

todot list [TtWwDd]     # List all tasks, their statuses, and their streaks.
                        # Add a letter to specify (T)ODO, (W)ORK, or (D)ONE tasks.
todot remind            # Send a notification if there are incomplete tasks.
todot streak            # Update the streak info for each task.
                        # DONE tasks are incremented by 1.
                        # TODO and WORK tasks are reset to 0.
todot dmenu             # Use the dmenu interface to add, delete, or mark tasks.

todot add               # Adds a new task.
todot delete            # Deletes a task.

todot todo              # Marks a task as TODO.
todot work              # Marks a task as WORK.
todot done              # Marks a task as DONE.
```

## Requirements

+ Requires GNU sed, GNU awk, notify-send, and dmenu for the interface.

## Upcoming features

+ Perhaps a daemon? So you wouldn't need to use crontabs to handle notifications, clearing the list every day, etc. Shouldn't be too hard.
